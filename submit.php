<?php
    if(isset($_POST['submit']))
    {
        $name = $_POST['name']; // Get Name value from HTML Form
        $email_id = $_POST['email']; // Get Email Value
        $msg = $_POST['message']; // Get Message Value

        $to = "rijamg@gmail.com"; // You can change here your Email
        $subject = "'$name' a envoyé un mail"; // This is your subject

        $google_recaptcha_secret = "6LdH8RkcAAAAADo24c4Xcyuby9lGlv9rDqK52W8J";
        $api_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$google_recaptcha_secret."&response=".$_POST['g-recaptcha-response']);
        $api_response = json_decode($api_response, true);
        // HTML Message Starts here
        $message ="
        <html>
            <body>
                <table style='width:600px;'>
                    <tbody>
                        <tr>
                            <td style='width:150px'><strong>Name: </strong></td>
                            <td style='width:400px'>$name</td>
                        </tr>
                        <tr>
                            <td style='width:150px'><strong>Email ID: </strong></td>
                            <td style='width:400px'>$email_id</td>
                        </tr>
                        <tr>
                            <td style='width:150px'><strong>Message: </strong></td>
                            <td style='width:400px'>$msg</td>
                        </tr>
                    </tbody>
                </table>
            </body>
        </html>
        ";


        // HTML Message Ends here

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: Site <tovo@partenairenumerik.com>' . "\r\n"; // Give an email id on which you want get a reply. User will get a mail from this email id


        if($api_response["success"] === true)
            {
                // success result google Invisible reCAPTCHA v2
                if(mail($to,$subject,$message,$headers)){
                          // Message if mail has been sent
                          echo "<script>
                                  alert('Votre message a bien été envoyé. Merci!!!');
                                  window.location.href = 'index.html';
                              </script>";
                      }

                      else{
                          // Message if mail has been not sent
                          echo "<script>
                                  alert('EMAIL FAILED');
                              </script>";
                      }
            }
            else
            {
                // not success result google Invisible reCAPTCHA v2
                echo "You are a robot";
            }
    }
?>
