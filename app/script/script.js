let spinnerWrapper = document.querySelector('.spinner-wrapper');

    window.addEventListener('load', function () {
        spinnerWrapper.parentElement.removeChild(spinnerWrapper);
    });

document.addEventListener("DOMContentLoaded", ()=>{

  let hamburger = document.querySelector('.hamburger');
  let topheader = document.querySelector('.top-header');
  let menu = document.querySelector('.menu');
  let body = document.querySelector('body');

  hamburger.addEventListener('click', function(){
    if (topheader.classList.contains('open')){
      topheader.classList.remove('open');
      menu.classList.remove('selected');
      body.classList.remove('noscroll');
    }
    else {
      topheader.classList.add('open');
      menu.classList.add('selected');
      body.classList.add('noscroll');
    }
  })
})
